# Databricks notebook source
# MAGIC %md 
# MAGIC A Python Notebook task that requires input parameters and demonstrates how to copy the deployed .jar from the mlFlow artifact location to a mounted S3 bucket

# COMMAND ----------

dbutils.widgets.text("WIDGET_BUCKET_PATH", "/mnt/changebucket", "Mount Path for S3 Bucket")
dbutils.widgets.text("MLFLOW_EXPERIMENT_NAME", "/Shared/dbx/projects/dbx-scala-example-gitlab", "mlFlow Experiment")

# COMMAND ----------

DROP_BUCKET_PATH = getArgument("WIDGET_BUCKET_PATH")
print("Will copy files to", DROP_BUCKET_PATH)

dbutils.fs.ls(DROP_BUCKET_PATH)

# COMMAND ----------

EXPERIMENT_NAME = getArgument("MLFLOW_EXPERIMENT_NAME")

print("Will retrieve artifacts from mlFlow experiment:", EXPERIMENT_NAME)

# COMMAND ----------

import mlflow
from datetime import datetime

# COMMAND ----------

#First we want to retrieve the most recent successful mlFlow deployment run, so that we can identify the artifact location
general_filters = [
    "tags.dbx_status = 'SUCCESS'",
    "tags.dbx_action_type = 'deploy'",
]
filter_string = " and ".join(general_filters)

runs = mlflow.search_runs([mlflow.get_experiment_by_name(EXPERIMENT_NAME).experiment_id], filter_string=filter_string, order_by=["start_time DESC"])

if runs.empty:
  raise Exception("No mlFlow runs were identified, no artifacts will be copied to the S3 Drop Bucket")
  
display(runs)

# COMMAND ----------


run_info = runs.iloc[0].to_dict()

#we will create a new folder in the drop bucket with the current data/time
NEW_DIR = datetime.now().strftime('%Y%m%d%H%M')

#we will identify all .jar files in the artifact location 
copy_files = [f for f in dbutils.fs.ls("//".join([run_info['artifact_uri'],"target"])) if f.name.endswith(".jar")]

if len(copy_files)>0:
  for f in copy_files:
    new_file_path = "//".join([DROP_BUCKET_PATH, NEW_DIR,f.name])
    dbutils.fs.cp (f.path, new_file_path)
    print("Copied the file from:", f.path, " to ", new_file_path)  
else:
  print("No .jar files found to copy")
  

# COMMAND ----------


