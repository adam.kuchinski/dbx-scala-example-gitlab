// Databricks notebook source
import net.kuch.demos.DateDimension

// COMMAND ----------

val dateDim = new DateDimension().createDataFrame()

dateDim.write.mode("overwrite").saveAsTable("scala_test")

// COMMAND ----------

// MAGIC %sql 
// MAGIC 
// MAGIC select * from scala_test

// COMMAND ----------

// MAGIC %md 
// MAGIC ##Check For Duplicates
// MAGIC * Should return zero

// COMMAND ----------

// MAGIC %sql
// MAGIC select date_key, count(*) 
// MAGIC from scala_test
// MAGIC group by date_key
// MAGIC having count(*) > 1 

// COMMAND ----------

// MAGIC %md ##Check for Leap Years

// COMMAND ----------

// MAGIC %sql
// MAGIC select year, count(*) as days_in_year
// MAGIC from scala_test
// MAGIC group by year
// MAGIC having days_in_year > 365
// MAGIC order by year  

// COMMAND ----------


