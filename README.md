# Sample Scala project for Databricks with dbx support to run CI/CD pipelines on GitLab

Sample project for Scala 2.12 and Spark 3.1.1 with [`dbx`](https://github.com/databrickslabs/dbx) support, based on some ideas from [cicd-templates](https://github.com/databrickslabs/cicd-templates).

cicd-templates is a Python project template that uses cookiecutter to create CI/CD pipeline code for different cloud and CI/CD tool combinations.  It uses dbx and the databricks-cli.  

[dbx-scala-example](https://github.com/renardeinside/dbx-scala-example) is a Scala project that does not use cicd-templates, but borrows some ideas for an example pipeline to run in GitHub Actions.

This project further evolves dbx-scala-example with the following additions/changes:
- Change CI/CD tool from GitHub to GitLab
- Use scalatest for local unit tests, integrated with Maven
- Provide additional example Databricks jobs
  - A Scala Notebook task that imports the library from the deployed .jar and runs some integration tests
  - A Python Notebook task that requires input parameters and demonstrates how to copy the deployed .jar from the mlFlow artifact location to a mounted S3 bucket
  - A Multi-task job that chains together two sequential tasks 

## Credits
https://github.com/renardeinside/dbx-scala-example

https://github.com/pohlposition/datamart

https://github.com/holdenk/spark-testing-base


## Sample project structure
```
.
├── .dbx
│   └── project.json
├── .gitignore
├── README.md
├── .gitlab-ci.yml
├── conf
│   └── deployment.json
├── notebooks
│   └── <Databricks Notebook Files>
├── src
│   └── main
│   │   └── scala
│   │       └── <Scala Class and Object Files>
│   └── test
│       └── scala
│           └── <Scala Unit Test Files>
├── target
│   └── <maven build directory, not pushed to remote repo>
├── tools
│   └── <dbx wheel>
├── unit-requirements.txt
└── pom.xml
```

Some explanations regarding structure:
- `.dbx` folder is an auxiliary folder, where metadata about environments and execution context is located.
- `conf/deployment.json` - deployment configuration file. Please read [this section](https://github.com/databrickslabs/cicd-templates#deployment-file-structure) for a full reference.
- `.gitlab-ci.yml` - GitLab CI/CD workflow definition
- `target` will get created after running the Maven build, and scalatest results will be stored in `target/surefire-reports`

## Initial Config
You must have the databricks-cli installed and configured in an environment with Python 3.6+
  - [Instructions here](https://docs.databricks.com/dev-tools/cli/index.html)
  - If this is the first time that you are using the databricks-cli on this machine, be sure to complete the authentication with your workspace host and personal token
    - You can setup multiple profiles if you have more than one Databricks workspace
  - Test that the CLI is properly configured by running a command such as:
    - ` databricks clusters list`
    - or `databricks clusters list --profile <name of your profile from ~/.databrickscfg>`
    
Install `dbx`:
```
pip install dbx
```


Now you may want/need to make some updates to the code configuration
  - `.dbx/project.json`
    - Update `profile` to the name of your desired workspace profile from ~/.databrickscfg
    - Update `workspace_dir` to the location in your workspace where you'd like to store the MLFlow Experiment
    - Update `artifact_location` to the location in dbfs where you'd like to deploy the files and libraries that will be referenced by the jobs that get created
  - `conf/deployment.json`
    - The `dbx-scala-example` job will create a spark_jar_task using the jar and main class specified.  You will want to configure this as necessary, and also potentially change the cluster settings.
    - The `dbx-notebook-test` job will create a notebook task, but we will need to upload the notebook separately, dbx does not currently handle notebook deployment.  Further instructions in the **Notebook Job** section below.
      - You may want to update the `notebook_path` for this job.  If you change it later, you will just need to rerun `dbx deploy` to update the job configuration
  - `pom.xml`
    - You will need to have Maven configured in your environment.  Check the pom to edit the repositories, dependencies, and versions of spark, scala, etc... as needed.



## Build, Test, and Deploy
Now use Maven to build the .jar and run the [scalatest](https://www.scalatest.org/user_guide/using_the_scalatest_maven_plugin) in net.kuch.demos.DateDimensionTest.  Can use the Maven plugin in an IDE like IntelliJ IDEA or execute from the command line:
```
mvn package
```

Once the .jar has been built, and the unit tests have passed, we can run the dbx deploy.  This will:
  - Create the jobs in Databricks that were defined in `conf/deployment.json`
  - Create an MLFlow Experiment in the `workspace_dir` location 
  - Deploy the artifacts needed by the jobs to the `artifact_location`

https://dbx.readthedocs.io/en/latest/cli.html#dbx-deploy

```
dbx deploy --no-rebuild --no-package 
```

Note:  for future deploy commands, we could add --files-only.  This wouldn't update the job definitions but would deploy the updated artifacts.

## Run Jobs

### Scala Jar Job 
Since the .jar and scala src files have already been deployed to dbfs by dbx, we can launch the `dbx-scala-example` job.  You will be able to view the progress of the job in the Databricks UI.
```
dbx launch --job dbx-scala-example --trace
```

### Notebook Job

`dbx v0.1.5` does not deploy notebooks automatically.  So if we want to run a job with a Notebook Task, we need to manually deploy the notebook file. You could do this through the UI and then update the `notebook_path` for the job in `conf/deployment.json`.  Or you could use the databricks-cli, which will allow you to make local updates to your notebook and commit using git.  Here is an example workflow:
```
databricks workspace import_dir ./notebooks /Shared/dbx/notebooks
```

Now `date_dimension_test` is available in the same location specified for the job in the deployment json, and we can launch the `dbx-notebook-test` job.
```
dbx launch --job dbx-notebook-test --trace
```

To close this loop, you could now make changes to your Notebook in the Databricks UI, and export the files back to your local workspace so that you can use git for diffs and commits.
```
databricks workspace export_dir --overwrite /Shared/dbx/notebooks ./notebooks
```

(You can also import and export files instead of directories: https://docs.databricks.com/dev-tools/cli/workspace-cli.html)

### Python Notebook Job to Copy .jar to S3 bucket

The job defined as `dbx-notebook-jar-copy` will execute the python notebook `jar_copy_to_s3`.
- This task provides an example of how to access the libraries that are deployed to the artifact location, and shows how to copy .jar files from dbfs to a different S3 bucket.
- Follow the instructions in the section above to import the notebook to your Databricks workspace.
- Note that this notebook has two widgets defined, and you should update the `base_parameters` in `notebook_path` to provide the proper values to these widgets.
  - `MLFLOW_EXPERIMENT_NAME` should be set to the same path as the `workspace_dir` in `project.json`. 
  - `WIDGET_BUCKET_PATH` should be the /mnt path for the S3 bucket that the .jar artifacts will be copied to.  This S3 bucket needs to be [properly mounted in Databricks](https://docs.databricks.com/data/data-sources/aws/amazon-s3.html) first.



## Integration with GitLab Pipelines

Now that we have the project configured locally, and the Jobs deployed and tested in Databricks, we can commit the code to GitLab and initialize the CI/CD Pipeline Jobs in GitLab. These jobs will automatically launch the Maven build (with scalatests) and Databricks jobs when code changes are pushed to the remote repository, and will notify you if the commit broke the pipeline.

Here are the basic instructions, as outlined in [cicd-templates](https://github.com/databrickslabs/cicd-templates#setting-up-cicd-pipeline-on-gitlab)
- Create a new repository on Gitlab
- Configure DATABRICKS_HOST and DATABRICKS_TOKEN secrets for your project in [GitLab UI](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project)
- Add a remote origin to the local repo
- Push the code
- Open the GitLab CI/CD UI to check the deployment status

Some further notes:
- The GitLab Pipeline steps are defined in .gitlab-ci.yml
- We have chosen the [openkbs/jdk-mvn-py3](https://hub.docker.com/r/openkbs/jdk-mvn-py3) Docker image because it has both Maven and Python3 installed
- Review the options for running Jobs in your pipeline as either [Run Submit](https://docs.databricks.com/dev-tools/api/latest/jobs.html#runs-submit) or [Run Now](https://docs.databricks.com/dev-tools/api/latest/jobs.html#run-now):  
  - https://github.com/databrickslabs/cicd-templates#different-deployment-types
- If desired, you could edit the Gitlab pipeline to programmatically push Notebooks needed for notebook_tasks to a staging folder in the Databricks workspace using the databricks-cli Workspace API.
- You could [get the output of a run](https://docs.databricks.com/dev-tools/api/latest/jobs.html#runs-get-output) and take further actions based on that. 